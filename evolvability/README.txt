We implement our algorithm in Python language and more precisely we use Jupyter notebooks. 
Our main class is the evolutionary_algorithm.ipynb file where we run our algorithm for 100 epochs. 
If you want to change that, in line 184 you simply replace the number 100 to the one you prefer.
Additionally, we have created two other .ipynb files, that create the necessary .xlsx files. 
For your convenience, we have created a seperate directory results_total,
where the results from the file evolutionary_algorithm are saved with the name results.xlsx.
Also we have two other .ipynb files script_excel_avg_var_values.ipynb and script_excel_files.ipynb,
which are useful if you want to create more specific .xlsx files considering the results.xlsx, that
algorithm creates.
If you want to run script_excel_avg_var_values.ipynb and script_excel_files.ipynb, firstly
create two directories inside the project evolvability, with names results_probabilities and results_avg_var.
Also the order by which you execute the classes is :
1) evolutionary_algorithm.ipynb,
2) script_excel_files.ipynb
3) script_excel_avg_var_values.ipynb


